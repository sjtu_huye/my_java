package Practice;
/*
 * Copyright (c) SJTU Technologies Co., Ltd. 2022-2022. All rights reserved.
 * Description:
 * Author: sjtu_huye
 * Create: 2022/3/18
 */

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class l0005_LongestPalindromeTest {
    public int caseCnt = 0;
    @BeforeMethod
    public void beforeMethod() {
        System.out.println("*************************");
        System.out.println("Case: " + this.caseCnt);
        this.caseCnt++;
    }
    
    @DataProvider(name = "provider")
    public static Object[][] numbers() {
        return new Object[][]{
            {"abc", "a"},
            {"aaa", "aaa"},
            {"abcddsdda", "ddsdd"},
            {"abccef", "cc"}
        };
    }
    
    @Test(dataProvider = "provider")
    public void test1(String s, String n) {
        System.out.println("Str: " + s + ", palindrome: " + n);
        l0005_LongestPalindrome procFunc = new l0005_LongestPalindrome();
        String out = procFunc.solution(s);
        Assert.assertEquals(out, n);
    }
    
    
    @Test(dataProvider = "provider")
    public void test2(String s, String n) {
        System.out.println("Str: " + s + ", palindrome: " + n);
        l0005_LongestPalindrome procFunc = new l0005_LongestPalindrome();
        String out = procFunc.solution2(s);
        Assert.assertEquals(out, n);
    }
    
    @AfterMethod
    public void afterMethod() {
        System.out.println("case end");
        System.out.println("*************************");
    }
}