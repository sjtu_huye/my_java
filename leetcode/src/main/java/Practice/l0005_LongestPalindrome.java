/*
 * Copyright (c) SJTU Technologies Co., Ltd. 2022-2022. All rights reserved.
 * Description:
 * Author: sjtu_huye
 * Create: 2022/3/18
 */


package Practice;
import java.util.ArrayList;
import java.util.List;


public class l0005_LongestPalindrome {
    public String solution(String s) {
        /*
            动态规划
            p(i,j) i~j是否为回文子串
            p(i,j) = p(i+1,j-1) ^ (si == sj)
            初值为每个单独的字符都是回文子串
            遍历查询2个为回文子串的，然后标记，然后再3个、4个、5个，类似滑窗判断
        */
        int maxLen = 1;
        int strLen = s.length();
        int startPos = 0;
        if (strLen < 2) {  // 空则直接返回
            return s;
        }
        boolean[][] dp = new boolean[strLen][strLen];
        char[] charArr = s.toCharArray();
//        for (int i = 0; i < strLen; i++) {
//            dp[i][i] = true;
//        }
        for (int l = 2; l <= strLen; l++) { // 左右间隔，至少为1、2这种相差1
            for (int i = 0; i < strLen; i++) {
                int j = l + i - 1;
                if (j >= strLen) break;
                if (charArr[i] != charArr[j]) {
                    dp[i][j] = false;
                } else {
                    if (j - i < 3) {  // 小于3的话，前面的初始化可以不用赋值
                        dp[i][j] = true;
                    } else {
                        dp[i][j] = dp[i + 1][j - 1];
                    }
                }
                if (dp[i][j] && (j - i + 1 > maxLen)) {
                    maxLen = j - i + 1;
                    startPos = i;
                }
            }
        }
        return s.substring(startPos, startPos + maxLen);
    }
    
    public int getMaxLen(String s, int l, int r) {
        while (l >= 0 && r < s.length() && s.charAt(l) == s.charAt(r)) {
            l--;
            r++;
        }
        return r - l - 1;
    }
    
    public String solution2(String s) {
        /*
            中心扩展法
            以每个点为中心扩展
        */
        int strLen = s.length();
        int maxLen = 1;
        if (strLen < 2) {
            return s;
        }
        int startPos = 0;
        for (int i = 0; i < strLen; i++) {
            int l1 = getMaxLen(s, i, i);
            int l2 = getMaxLen(s, i, i+1);
            int l = Math.max(l1, l2);
            if (l > maxLen) {
                startPos = i + 1 - (l + 1) / 2;
                maxLen = l;
            }
        }
        return s.substring(startPos, startPos + maxLen);
    }
}